const express = require(`express`);
const app = express();
const port = 3010;

const mongoose = require(`mongoose`);
const dotenv = require(`dotenv`);
const e = require("express");
dotenv.config();

// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true})) 

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})

const db = mongoose.connection;
db.on(`error`, console.error.bind(console, `connection error`))
db.once(`open`, () => console.log(`Connected to Database`))

// solution #1
const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, `Name is required`]
        }, 
        password: {
            type: String,
            required: [true, `Password is required`]
        }
    }
) 

// solution #2
const user = mongoose.model(`user`, userSchema)

app.post(`/signup`, (req, res) => {
    // console.log(req.body)
    const user = user.findOne({name: req.body.name})
    .then((user, err) => {console.log(user)})
    console.log(user) // a document

    if(user != null && user.name == req.body.name){
        return res.send(`User ${req.body.name} was created.`)
    } else {
        let newUser = new user({
            name: req.body.name
        })
        newUser.save().then((savedUser, err) => {
            if(savedUser){
                return res.send(savedUser)
            } else {
                return res.status(500).send(err)
            }
        })
    }
    
})

app.listen(port, () => console.log(`Server connected at port ${port}`));